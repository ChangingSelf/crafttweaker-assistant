import React from 'react';

import './App.css';
import { Row, Col, Space } from "antd";

import CraftTable from './components/CraftTable';

function App() {
  return (
    <div className="App">
      <Row>
        <Col span={8}>

        </Col>
        <Col span={8}>
          <CraftTable />
        </Col>
        <Col span={8}>

        </Col>
      </Row>
    </div>
  );
}

export default App;

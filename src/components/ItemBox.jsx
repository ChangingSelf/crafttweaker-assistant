import React, { Component } from "react";
import { Avatar } from "antd";
import { CodeSandboxOutlined } from "@ant-design/icons";
import ItemStack from "../entity/ItemStack";
import { Tooltip } from 'antd';

/**
 * 物品框，用于在合成表中显示物品
 */
class ItemBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      itemStack:props.itemStack || new ItemStack(),
    };
  }

  componentWillReceiveProps(nextProps){
    this.setState({
        itemStack:nextProps.itemStack || new ItemStack()
    });
  }

  render() {
    return (
      <Tooltip title={this.state.itemStack.toString()}>
        
        <Avatar
          shape="square"
          size="large"
          src={this.state.itemStack.texture}
          icon={<CodeSandboxOutlined />}
          {...this.props}
        >
          {this.state.itemStack.itemName}
        </Avatar>
      </Tooltip>
    );
  }
}

export default ItemBox;

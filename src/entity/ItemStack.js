
/**
 * 物品堆
 */
class ItemStack {
    /**
     * 
     * @param {string} modName mod名称，原版则为'minecraft'
     * @param {string} itemName 物品名称，如'egg'
     * @param {string} texture 材质图片url
     * @param {int} amount 物品数目
     */
    constructor(modName = 'minecraft', itemName = 'air', texture = '', amount = 1) {
        this.modName = modName;//mod名称，原版则为'minecraft'
        this.itemName = itemName;//物品名称，如'egg'
        this.texture = texture;//材质url
        this.amount = amount;//物品数目
    }

    toString() {
        if (this.itemName && this.modName) {
            return `<item:${this.modName}:${this.itemName}>`;
        } else {
            return '';
        }

    }
}

export default ItemStack;
# crafttweaker-assistant

minecraft 的一个名为[CraftTweaker](https://www.mcmod.cn/class/669.html)的 mod，允许你使用简单的脚本语言来自定义你的整合包或服务器，可以通过一种名为 ZenScript 的脚本语言来定义合成表。

本项目的目的是为 ZenScript用React+Antd+Electron写一个 GUI，方便使用者自定义合成表

设计详情见：[项目介绍博文](https://yxchangingself.xyz/posts/CraftTweaker-Assistant/)

![初版原型](https://i.loli.net/2021/01/03/Uu7eNTKkPZjtFH4.png)

![初版成品](https://i.loli.net/2021/01/12/HSCBWq5PdrJ3UEV.png)

# 参考链接

[CraftTweaker 官方中文文档](https://docs.blamejared.com/1.16/zh/getting_started/)

[CraftTweaker 的 github 仓库](https://docs.blamejared.com/1.16/zh/getting_started/)

# json 文件格式

```json
{
  "mod名": {
    "物品名": {
      "texture": "物品材质图片url"
    }
  }
}
```
